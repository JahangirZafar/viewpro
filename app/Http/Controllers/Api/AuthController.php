<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\User;
use Aws\Sns\Exception\SnsException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;


class AuthController extends Controller
{
    public function login(Request $request)
    {
        if ($request->input('email')) {
            $email = $request->input('email');
            $device_id = $request->input('device_id');
            $status = false;
            $exception = "";
            $user = User::where('email', $email)->first();
            if ($user) {
                $status = true;
                try {
                    $deviceToken = null;
                    if ($deviceToken == null) {
                        $platformApplicationArn = "arn:aws:sns:us-east-1:727732256569:app/APNS_VOIP_SANDBOX/AppleVoip";//env('arn:aws:sns:us-east-1:727732256569:app/APNS_VOIP_SANDBOX/AppleVoip');
                        $client = App::make('aws')->createClient('sns');
                        $result = $client->createPlatformEndpoint(array(
                            'PlatformApplicationArn' => $platformApplicationArn,
                            'Token' => $device_id,
                        ));
                        $endPointArn = isset($result['EndpointArn']) ? $result['EndpointArn'] : '';
                        $user->device_id = $device_id;
                        $user->device_arn = $endPointArn;
                        $user->save();
                        DB::update("update users set device_id='$device_id', device_arn='$endPointArn' where email=?", ["$email"]);
                    }
                } catch (SnsException $e) {
                    $status = false;
                    $exception = $e->getMessage();
                }
                if ($status) {
                    return response()->json([
                        'status' => true,
                        'message' => 'successfully logged in.'
                    ], 200);
                } else {
                    return response()->json([
                        'status' => false,
                        'message' => $exception
                    ], 200);
                }
            }
        }
    }

    public function updateToken(Request $request)
    {
        $email = $request->input('email');
        $device_id = $request->input('device_id');
        $status = false;
        $exception = "";
        $user = User::where('email', $email)->first();
        if($user){
            try {
                $deviceToken = null;
                if ($deviceToken == null) {
                    $platformApplicationArn = "arn:aws:sns:us-east-1:727732256569:app/APNS_VOIP_SANDBOX/AppleVoip";//env('arn:aws:sns:us-east-1:727732256569:app/APNS_VOIP_SANDBOX/AppleVoip');
                    $client = App::make('aws')->createClient('sns');
                    $result = $client->createPlatformEndpoint(array(
                        'PlatformApplicationArn' => $platformApplicationArn,
                        'Token' => $device_id,
                    ));
                    $endPointArn = isset($result['EndpointArn']) ? $result['EndpointArn'] : '';
                    $user->device_id = $device_id;
                    $user->device_arn = $endPointArn;
                    if($user->save()){
                        $status = true;
                    }

                    //DB::update("update users set device_id='$device_id', device_arn='$endPointArn' where email=?", ["$email"]);
                }
            } catch (SnsException $e) {
                $status = false;
                $exception = $e->getMessage();
            }
        }

        if($status){
            return response()->json([
                'status' => true,
                'message'=> 'successfully updated'
            ], 200);
        }else {
            return response()->json([
                'status' => false,
                'message'=> $exception
            ], 200);
        }
    }
}
