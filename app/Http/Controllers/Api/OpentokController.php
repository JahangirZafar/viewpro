<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Aws\Sns\Exception\SnsException;
use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use OpenTok\OpenTok;
use OpenTok\Role;




class OpentokController extends Controller
{
    use InteractsWithQueue, Queueable, SerializesModels;
    private $opentok;
    public function __construct()
    {
        $this->opentok = new OpenTok("46037042", "1e5718f7d59bcb41ef11540cc197912d970c7731");
    }

    public function createOpentokSession(Request $request)
    {
        $video_call = false;
        if($request->has('call_type') && $request->input('call_type') == 'video'){
            $video_call = true;
        }
		
        // creating session
        $session = $this->opentok->createSession();

        // generate token for publisher
        $pubishToken = null;
        $subsToken = null;
        if($session){
            $pubishToken = $session->generateToken([
                'role'       => Role::PUBLISHER,
                'expireTime' => time()+(7 * 24 * 60 * 60), // in one week
            ]);
            $subsToken = $session->generateToken([
                'role'       => Role::SUBSCRIBER,
                'expireTime' => time()+(7 * 24 * 60 * 60), // in one week
            ]);
        }

        // send push to ios
        $this->sendVoipPush($session->getSessionId(), $pubishToken, $video_call);

        // response
        return response()->json([
           'status' => true,
           'sessionId' => $session->getSessionId(),
           'tokenId' => $subsToken,
           'publishToken' => $pubishToken,
           //'push' => $push
        ], 200);
    }

//    public function sendPush($sessionId, $publishToken, $video_call)
//    {
//        //$authkey = 'AIzaSyBfkC8r8c2YXxQlYe4k-lvFnneMGCg0UB0';
//        //$authkey = 'AIzaSyAqABzYNDG-I9Cgrzuyx-rE0dzFgxgTfh8'; //viewpro
//
//        $authkey = "AAAA7aBJcTs:APA91bHsaRe1VLWoAbFlH9KYjL_SzzGyquBq6n9JDU8Shm3vTYUgEUe7vqfAH5EZjbT9FwpFUHU84XFWepVlL34BCCKVVIbeGJ0yE6YLe37VjCdOByCY7ZSLLYXH_sHyqjLkhoy6_d7B";
//
//
//        $gcm_ios_mobile_reg_key = array();
//        $users = DB::select('select device_id from users');
//        foreach ($users as $user){
//            if($user->device_id){
//                $gcm_ios_mobile_reg_key[] = $user->device_id;
//            }
//        }
//
//        //return $gcm_ios_mobile_reg_key; exit;
//        //$gcm_ios_mobile_reg_key[] = "dBE78x4bEng:APA91bEhDemOdSRRdBaKmX_OqHTD-T7htIywKiQPQNrrzJ2N-QaRFSYWCf7iX9w_5quurdyZZKxKGnoNNC3-KjrMwr0HmSzmpHet4OBTCOH1JVgBgoC6qsuWDVe2VQWToGFML6FZe_Uc";
//
//        $fields = array(
//            "registration_ids" => $gcm_ios_mobile_reg_key, //1000 per request logic is pending
//            "priority" => "high",
//            "data" => array(
//                "sessionId" => $sessionId,
//                "publishToken" => $publishToken,
//                "video" => $video_call,
//            ),
//            'notification' => [
//                "body" => "Body Message",
//                "title" => "Someone Calling",
//                "sound" => "default"
//            ]
//        );
//
//        //$url = 'https://gcm-http.googleapis.com/gcm/send'; //note: its different than android.
//        $url = 'https://fcm.googleapis.com/fcm/send'; //note: its different than android.
//
//
//        $headers = array(
//            'Authorization: key='.$authkey,
//            'Content-Type: application/json'
//        );
//
//        $ch = curl_init();
//
//        // Set the url, number of POST vars, POST data
//        curl_setopt($ch, CURLOPT_URL, $url);
//
//        curl_setopt($ch, CURLOPT_POST, true);
//        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//
//        // Disabling SSL Certificate support temporarly
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//
//        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
//
//        // Execute post
//        $result = curl_exec($ch);
//        if ($result === FALSE) {
//            die('Curl failed: ' . curl_error($ch));
//        }
//        curl_close($ch);
//        return $result;
//    }



//    public function getDeviceToken(Request $request)
//    {
//        //$input = $request->only('user_id', 'platform', 'device_token');
//        $input['plateform'] = "IOS";
//        $input['device_id'] = "FD5E6B3040C7AAF8BACD5FFD64F85F78F2A15602C49E85F97A81AD1504274E5C";
//
//        try {
//            $deviceToken = null;//UserDeviceToken::whereDeviceToken($input['device_token'])->first();
//            if ($deviceToken == null) {
//                //$platformApplicationArn = '';
//                //if (isset($input['platform']) && $input['platform'] == 'IOS') {
//                    $platformApplicationArn = "arn:aws:sns:us-east-1:727732256569:app/APNS_VOIP_SANDBOX/AppleVoip";//env('arn:aws:sns:us-east-1:727732256569:app/APNS_VOIP_SANDBOX/AppleVoip');
//                //}
//                $client = App::make('aws')->createClient('sns');
//                $result = $client->createPlatformEndpoint(array(
//                    'PlatformApplicationArn' => $platformApplicationArn,
//                    'Token' => $input['device_id'],
//                ));
//                $endPointArn = isset($result['EndpointArn']) ? $result['EndpointArn'] : '';1;
//                $deviceToken = new User();
//                $deviceToken->device_id = $input['device_id'];
//                $deviceToken->device_arn = $endPointArn;
//            }
//            //$deviceToken->user_id = $input['user_id'];
//            $deviceToken->save();
//        } catch (SnsException $e) {
//            echo $e->getMessage();
//            return response()->json(['error' => "Unexpected Error"], 500);
//        }
//        return response()->json(["status" => "Device token processed"], 200);
//    }


    public function sendVoipPush($sessionId, $publishToken, $video_call)
    {
        $userDeviceTokens = User::get();

        foreach ($userDeviceTokens as $userDeviceToken) {
            $deviceToken = $userDeviceToken->device_id;
            $endPointArn = array("EndpointArn" => $userDeviceToken->device_arn);
            try {
                $sns = App::make('aws')->createClient('sns');
                $endpointAtt = $sns->getEndpointAttributes($endPointArn);
                if ($endpointAtt != 'failed' && $endpointAtt['Attributes']['Enabled'] != 'false') {
                    //if ($deviceToken->platform == 'android') {
                        $payload = ["alert" => 'Someone Calling', "sound" => 'default'];

                        $message = '{"APNS_VOIP_SANDBOX": "{\"aps\":{\"sessionId\": \"'.$sessionId.'\", \"publishToken\": \"'.$publishToken.'\", \"video\": \"'.$video_call.'\"} }"}';
                        $sns->publish([
                            'TargetArn' => $userDeviceToken->device_arn,
                            'Message' => $message,
                            'MessageStructure' => 'json'
                        ]);
                   // }
                }
                return true;
            } catch (SnsException $e) {
                Log::info($e->getMessage());
            }
        }
    }
	public function sendvideoPush($sessionId="", $publishToken="", $video_call=true)
    {
		 $session = $this->opentok->createSession();
		$sessionId=$session->getSessionId();
		$publishToken=$session->generateToken([
                'role'       => Role::PUBLISHER,
                'expireTime' => time()+(7 * 24 * 60 * 60), // in one week
            ]);
		
       $userDeviceTokens = User::get();
    foreach ($userDeviceTokens as $userDeviceToken) {
            $deviceToken = $userDeviceToken->device_id;
            $endPointArn = array("EndpointArn" => $userDeviceToken->device_arn);
            try {
                $sns = App::make('aws')->createClient('sns');
                $endpointAtt = $sns->getEndpointAttributes($endPointArn);
                if ($endpointAtt != 'failed' && $endpointAtt['Attributes']['Enabled'] != 'false') {
                    //if ($deviceToken->platform == 'android') {
                        $payload = ["alert" => 'Someone Calling', "sound" => 'default'];

                        $message = '{"APNS_VOIP_SANDBOX": "{\"aps\":{\"sessionId\": \"'.$sessionId.'\", \"publishToken\": \"'.$publishToken.'\", \"video\": \"'.$video_call.'\"} }"}';
                        $sns->publish([
                            'TargetArn' => $userDeviceToken->device_arn,
                            'Message' => $message,
                            'MessageStructure' => 'json'
                        ]);
                   // }
                }
                return true;
            } catch (SnsException $e) {
                Log::info($e->getMessage());
            }
        }
        
    }
public function sendaudioPush($sessionId="", $publishToken="", $video_call=false)
    {
		 $session = $this->opentok->createSession();
		$sessionId=$session->getSessionId();
		$publishToken=$session->generateToken([
                'role'       => Role::PUBLISHER,
                'expireTime' => time()+(7 * 24 * 60 * 60), // in one week
            ]);
		
       $userDeviceTokens = User::get();
    foreach ($userDeviceTokens as $userDeviceToken) {
            $deviceToken = $userDeviceToken->device_id;
            $endPointArn = array("EndpointArn" => $userDeviceToken->device_arn);
            try {
                $sns = App::make('aws')->createClient('sns');
                $endpointAtt = $sns->getEndpointAttributes($endPointArn);
                if ($endpointAtt != 'failed' && $endpointAtt['Attributes']['Enabled'] != 'false') {
                    //if ($deviceToken->platform == 'android') {
                        $payload = ["alert" => 'Someone Calling', "sound" => 'default'];

                        $message = '{"APNS_VOIP_SANDBOX": "{\"aps\":{\"sessionId\": \"'.$sessionId.'\", \"publishToken\": \"'.$publishToken.'\", \"video\": \"'.$video_call.'\"} }"}';
                        $sns->publish([
                            'TargetArn' => $userDeviceToken->device_arn,
                            'Message' => $message,
                            'MessageStructure' => 'json'
                        ]);
                   // }
                }
                return true;
            } catch (SnsException $e) {
                Log::info($e->getMessage());
            }
        }
        
    }
}
