var flag=false;


// create VoxImplant instance
var voxAPI = VoxImplant.getInstance();
// assign handlers
voxAPI.on(VoxImplant.Events.SDKReady, onSdkReady);
voxAPI.on(VoxImplant.Events.ConnectionEstablished, onConnectionEstablished);
voxAPI.on(VoxImplant.Events.ConnectionFailed, onConnectionFailed);
voxAPI.on(VoxImplant.Events.ConnectionClosed, onConnectionClosed);
voxAPI.on(VoxImplant.Events.AuthResult, onAuthResult);
voxAPI.on(VoxImplant.Events.MicAccessResult, onMicAccessResult);
voxAPI.on(VoxImplant.Events.SourcesInfoUpdated, onSourcesInfoUpdated);
 
// initialize SDK
try {
  voxAPI.init({ 
    micRequired: true, // force microphone/camera access request
    videoSupport: true, // enable video support 
    progressTone: true, // play progress tone
    localVideoContainerId: "callBox1", // element id for local video from camera or screen sharing
    remoteVideoContainerId: "callBox"
  });
} catch(e) {
  console.log(e);
}

// SDK ready - functions can be called now
function onSdkReady(){   
$('.buttons').hide();     
  console.log("onSDKReady version "+VoxImplant.version);
  console.log("WebRTC supported: "+voxAPI.isRTCsupported()); 
  connect();
}

// Connection with VoxImplant established
function onConnectionEstablished() {
  console.log("Connection established: "+voxAPI.connected());


  login();
  $('.buttons').show();

}

// login function
function login() {
  //console.log(username+"@"+application_name+"."+account_name+".voximplant.com");
  voxAPI.login("israr@test4.viewproapp.voximplant.com", "12345678");
}

// Connection with VoxImplant failed
function onConnectionFailed() {
  console.log("Connection failed");
  setTimeout(function() {voxAPI.connect();}, 1000);
}

// Connection with VoxImplant closed
function onConnectionClosed() {
  console.log("Connection closed");
  setTimeout(function() {voxAPI.connect();}, 1000);
}

// Handle authorization result
function onAuthResult(e) {
  console.log("AuthResult: "+e.result);
  
}

// Call's media element created
function onMediaElement(e) {
  // For WebRTC just using JS/CSS for transformation
  $video = $(e.element);
  $video.appendTo('#callBox');

  $video.css('width', '498px').css('height', '368px');
  $video[0].play();
  $("#ldr").hide();
  
   
}

// Call connected
function onCallConnected(e) {          
  console.log("CallConnected: "+currentCall.id());
 
    $('#cancelButton').click(function() {
      currentCall.hangup();
    }); 
  
  sendVideo(true);
  showRemoteVideo(true);  
  
}

function onaudioCallConnected(e) {          
  console.log("CallConnected: "+currentCall.id());
 
    $('#cancelButton').click(function() {
      currentCall.hangup();
    }); 
  
      $("#ldr").hide("slow");
	  $("#callBox").html("<h1> You are on Audio Call</h1>") 
  
}

// Call disconnected
function onCallDisconnected(e) {
  console.log("CallDisconnected: "+currentCall.id()+" Call state: "+currentCall.state());
  currentCall = null;
  $("#callModal").hide();
  $(".modal-backdrop").hide();
   


  
}

// Call failed
function onCallFailed(e) {
  console.log("CallFailed: "+currentCall.id()+" code: "+e.code+" reason: "+e.reason);
  $("#callModal").hide();
  $(".modal-backdrop").hide();
}

// Audio & video sources info available
function onSourcesInfoUpdated() {
     var audioSources = voxAPI.audioSources(),
      videoSources = voxAPI.videoSources();
}

// Camera/mic access result
function onMicAccessResult(e) {
  console.log("Mic/Cam access allowed: "+e.result);
  
}


// Progress tone play start
function onProgressToneStart(e) {
  console.log("ProgessToneStart for call id: "+currentCall.id()); 
}

// Progres tone play stop
function onProgressToneStop(e) {
  console.log("ProgessToneStop for call id: "+currentCall.id());  
}

// Create outbound call
function createvideoCall() {
	$('#callModal').show("slow");
	var loader = "<div class='loader' id='ldr'></div>";
 $('#callModal').modal({backdrop: 'static', keyboard: false});
                    // start loading
                    $('#callBox').html(loader);
  
  outboundCall = currentCall = voxAPI.call(
    'jahangir', 
    { receiveVideo: true, sendVideo: true }, 
    "TEST CUSTOM DATA"
  );

  $('#cancelButton').click(function() {
      currentCall.hangup();
    }); 
  currentCall.on(VoxImplant.CallEvents.Connected, onCallConnected);
  currentCall.on(VoxImplant.CallEvents.Disconnected, onCallDisconnected);
  currentCall.on(VoxImplant.CallEvents.Failed, onCallFailed);
  currentCall.on(VoxImplant.CallEvents.MediaElementCreated, onMediaElement);

}
function createaudioCall() {
	$('#callModal').show("slow");
	var loader = "<div class='loader' id='ldr'></div>";
 $('#callModal').modal({backdrop: 'static', keyboard: false});
                    // start loading
   $('#callBox').html(loader);
  
  outboundCall = currentCall = voxAPI.call(
    'jahangir', 
    { receiveVideo: false, sendVideo: false }, 
    "TEST CUSTOM DATA"
  );
    $('#cancelButton').click(function() {
      currentCall.hangup();
    }); 
	 
  currentCall.on(VoxImplant.CallEvents.Connected, onaudioCallConnected);
  currentCall.on(VoxImplant.CallEvents.Disconnected, onCallDisconnected);
  currentCall.on(VoxImplant.CallEvents.Failed, onCallFailed);
  currentCall.on(VoxImplant.CallEvents.MediaElementCreated, onMediaElement);
}

 

// Close connection with VoxImplant      
function closeConnection() {
  voxAPI.disconnect();
    $("#callModal").hide();
  $(".modal-backdrop").hide();
}

// Establish connection with VoxImplant
function connect() {
  console.log("Establishing connection...");
  
  voxAPI.connect();
  
  }


// Show/hide local video
function showLocalVideo(flag) {
  voxAPI.showLocalVideo(flag);
}

// Show/hide remote video
function showRemoteVideo(flag) {
  currentCall.showRemoteVideo(flag);
}

// Start/stop sending video
function sendVideo(flag) {
  voxAPI.sendVideo(flag);
}
