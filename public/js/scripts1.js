$(document).ready(function () {
    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;


    var loader = "<div class='loader' id='ldr'></div>";

    var sessionId = null;
    var tokenId = null;

    $(document).on('click', '.make-call', function (e) {
        e.preventDefault();
        var call_type = $(this).attr('data-call');
        var constraints = {"audio":true,"video":false};
        // if(call_type == 'video'){
        //     constraints.video = f;
        //     constraints.audio = true;
        // }else {
        //     constraints.audio = true;
        // }
        if (navigator.getUserMedia) {
            // Request the camera.
            navigator.getUserMedia(constraints,

                // Success Callback
                function() {
                    $('#callModal').modal({backdrop: 'static', keyboard: false});
                    // start loading
                    $('#callBox').html(loader);
                    // get session & token
                    $.ajax({
                        dataType: 'json',
                        url: '/api/opentoksession',
                        type: 'POST',
                        data: {call_type: call_type},
                        success: function(data, textStatus, jqXHR) {
                            // When AJAX call is successfuly
                            console.log('AJAX call successful.');
                            if(data.status == true){
                                sessionId = data.sessionId;
                                tokenId   = data.publishToken;
                                initializeSession(sessionId, tokenId);
                            }
                            console.log(data);
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            // When AJAX call has failed
                            console.log('AJAX call failed.');
                            console.log(textStatus + ': ' + errorThrown);
                        },
                        complete: function() {
                            $('.make-call').removeClass('disabled');
                            // When AJAX call is complete, will fire upon success or when error is thrown
                            console.log('AJAX call completed');
                        }
                    });
                },

                // Error Callback
                function(err) {
                    // Log the error to the console.
                    alert(err.name + ": Please enable atleast microphone for this call." );
                }
            );

        } else {
            alert('Sorry, your browser does not support getUserMedia');
        }
    });

    // End call button
    $(document).on('click', '.end-call', function () {
        endSession(false,true);
    });
});

function handleError(error) {
    if (error) {
        alert(error.message);
    }
}

//var sessionId= "2_MX40NjAzNzA0Mn5-MTUxNjk1MDU1MDgyOH5MbjlIcWxjNGd4Rnh0Sk9SWlZnSmpuTUl-UH4"
//var token = "T1==cGFydG5lcl9pZD00NjAzNzA0MiZzaWc9OTQwNmY4NDc2NDczOTkyNjQwM2E1NDAwYmRkY2M1NDhmNGIzNTUxNzpzZXNzaW9uX2lkPTJfTVg0ME5qQXpOekEwTW41LU1UVXhOamsxTURVMU1EZ3lPSDVNYmpsSWNXeGpOR2Q0Um5oMFNrOVNXbFpuU21wdVRVbC1VSDQmY3JlYXRlX3RpbWU9MTUxNjk1MDU1MSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNTE2OTUwNTUxLjAzMjA5NjA5NTQ1OSZleHBpcmVfdGltZT0xNTE3NTU1MzUx"
//$('#callModal').modal({backdrop: 'static', keyboard: false});


var session = null;
function initializeSession(sessionId, token) {
        session = OT.initSession("46037042", sessionId);

        // Subscribe to a newly created stream
        session.on('streamCreated', function (event) {
            //document.getElementById("callBox").innerHTML = "";
            session.subscribe(event.stream, 'callBox', {
                insertMode: 'append',
                width: '100%',
                height: '100%'
            }, handleError);
        });


        session.on('connectionDestroyed', function connectionDestroyed(event) {
            endSession("Your call is ended.", false);
            console.log('connectionDestroyed', event.reason);
        });
        // session.on('sessionDisconnected', function sessionDisconnected(event) {
        //     //endSession("Your call is ended.", false);
        //     console.log('sessionDisconnected: ', event.reason);
        // });
        // session.on('streamDestroyed', function streamDestroyed(event) {
        //     //event.preventDefault();
        //     //endSession("Your call is ended.", false);
        //     console.log('streamDestroyed: ', event.reason);
        // });
        //
        // session.on('streamPropertyChanged', function streamDestroyed(event) {
        //     console.log('streamPropertyChanged: ', event.reason);
        // });


        //Create a publisher
        var publisher = OT.initPublisher('publisher', {
            insertMode: 'append',
            width: '0%',
            height: '0%',
            videoSource: null
        }, function (error) {
            if (error) {
                $(".modal").modal('hide');
                if (error.name == "OT_USER_MEDIA_ACCESS_DENIED") {
                    alert('Call required microphone access.');
                }
            }
        });
        publisher.publishVideo(false);

        // Connect to the session
        session.connect(token, function (error) {
            // If the connection is successful, initialize a publisher and publish to the session
            if (error) {
                handleError(error);
            } else {
                session.publish(publisher, handleError);
            }
        });
}

//initializeSession(sessionId, token);



function endSession(msg, end_call){
    if(msg && end_call == false){
        alert(msg);
    }
    $(".modal").modal('hide');
    session.disconnect();
}