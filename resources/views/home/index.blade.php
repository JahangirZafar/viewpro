@extends('layouts.app')

@section('title', 'Home')


@section('content')
    
  <div class="container">
        <div class="row">
            <div class="col-sm">
                <div class="col-inner">
                    <div class="list-image">
                        <img src="https://www.viewpro.com/imgs/car-1.jpg" class="img-responsive">
                    </div>
                    <h4>Lamborghini Cars UK</h4>
                    <div id="subscriber_1" class="subscriber"></div>
                    <div class="buttons">
                        <a href="javascript:void(0)" id="video_call_button" class="btn make-call" title="Video Call" data-call="video"><i class="fa fa-video" aria-hidden="true"></i></a>
                        <a href="javascript:void(0)" id="audio_call_button" class="btn make-call" title="Audio Call" data-call="audio"><i class="fa fa-headphones" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-sm">
                <div class="col-inner">
                    <div class="list-image">
                        <img src="https://www.viewpro.com/imgs/car-2.jpg" class="img-responsive">
                    </div>
                    <h4>Lamborghini Cars USA</h4>
                    <div id="subscriber_1" class="subscriber"></div>
                    <div class="buttons">
                        <a href="javascript:void(0)" class="btn make-call" title="Video Call" data-call="video"><i class="fa fa-video" aria-hidden="true"></i></a>
                        <a href="javascript:void(0)" class="btn make-call" title="Audio Call" data-call="audio"><i class="fa fa-headphones" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
 <div id="voximplant_container"></div>
    


    <!-- Modal -->
    <div class="modal fade" id="callModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel"><script type="text/javascript">document.getElementById("demo");</script></h4>
                    
                        
                    
                </div>
                <div class="modal-body" style="min-height:200px;">
                    <div id="callBox" style="width:auto;height:auto;text-align:center;">
					
					
					</div>
					
                </div>
                <div class="modal-footer">
                    <button type="button" id="cancelButton" class="btn btn-secondary end-call" data-dismiss="modal">End Call</button>
                    
                </div>
            </div>
        </div>
    </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://static.opentok.com/v2/js/opentok.js"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.0/js/all.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
 

 <script>

      $(document).ready(function(){        
        $('#video_call_button').click(function(){
          createvideoCall();
        });
		$('#audio_call_button').click(function(){
          createaudioCall();
        });
        
      });

    </script>
    
@endsection