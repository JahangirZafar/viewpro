<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('index');
Route::get('/users', 'HomeController@users')->name('index');
//Route::get('/devicetoken', 'Api\OpentokController@getDeviceToken');
Route::get('/sendpush', 'Api\OpentokController@sendVoipPush');
Route::get('/sendvideoPush', 'Api\OpentokController@sendvideoPush');
Route::get('/sendaudioPush', 'Api\OpentokController@sendaudioPush');




Route::get('/auth/myform', 'Api\AuthController@myform')->name('index');
Route::get('/auth/login', 'Api\AuthController@login')->name('index');
Route::get('/auth/ss', 'Api\AuthController@another_function');

