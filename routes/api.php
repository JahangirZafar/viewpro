<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

//Route::get('opentoksession', 'Api\OpentokController@createOpentokSession');
Route::post('opentoksession', 'Api\OpentokController@createOpentokSession');

// login
Route::post('auth/login', 'Api\AuthController@login');
Route::post('auth/update-token', 'Api\AuthController@updateToken');